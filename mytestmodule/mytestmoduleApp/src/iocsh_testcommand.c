
/* 
 * Juntong Liu
 *               2021.03.29  7:30 - 14:30
 *================================================== 
 *
 * This is an implementation of an IOC shell command within a e3 module, so an e3 IOC can use it.
 *
 * The command take two arguments, first is a string "name" and second is an integer "numloop"
 * When user type the command into the IOC shell, the string "name" will be printed out "numloop" times
 *
 * File name: iocsh_testcommand.c
 *
 * Something Like:
 *
 * IOC-shell-prompt >$: hello test-string  3
 *
 *       test-string
 *       test-string
 *       test-string
 *     
 */

#include <stdio.h>
#include <iocsh.h>
#include <epicsExport.h>

/* 
 * This is a function to carry out the ioc shell command when the command is typed into an 
 * IOC shell
 */
epicsShareFunc void hello(const char *name, int numloop)
{
	int i;
	for(i=0; i<numloop; i++)
	      printf("%s\n", name);
	
}

/* Define the shell command arguments */
static const iocshArg helloArg0 = {"username", iocshArgString};
static const iocshArg helloArg1 = {"numloop", iocshArgInt};
static const iocshArg *const helloArgs[] = { &helloArg0, &helloArg1 };

/* IOC shell command name and arguments passed to the command */
static const iocshFuncDef helloDef = {"hello", 2, helloArgs};

// Hook the call to the real hello function
static void helloCall(const iocshArgBuf *args){
	const char *username = args[0].sval;
	int numloop = args[1].ival;
	hello(username, numloop);
}

/* Register the command to the IOC shell */
static void mytestmoduleRegister(void)          // ==> 2 places, down, and dbd
{
	static int firstTime = 1;
	if (firstTime){
		firstTime = 0;
		iocshRegister(&helloDef, helloCall);
	}
}

/* Export the registration function to regiter a command */
epicsExportRegistrar(mytestmoduleRegister);
